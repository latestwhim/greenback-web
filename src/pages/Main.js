import React from 'react';
import PaddedContent from '../ui/PaddedContent'

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {temperature: ''};
    }

    componentDidMount() {
        console.log("m")
        //this.props.onEnter(this.props.routerProps);
    }

    componentWillReceiveProps(nextProps) {
        console.log("p")
    }

    render() {
        return <PaddedContent>
                    <p>
                    Greencast is an app that helps you plan your expenses realistically.
                    By forecasting your bank account balance for a year, you will see if your budget is going to work.
                    You'll be avoiding overdraft fees. You'll see where you need to cutback.
                    </p>
                </PaddedContent>;
    }
}

export default Main;