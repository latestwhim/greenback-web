import React from 'react';
import PaddedContent from '../ui/PaddedContent'
import { Header } from 'semantic-ui-react'

const HomePage = () => (
    <PaddedContent>
        <Header as="h3">Get Back in the Green</Header>
        <p>
        Greencast is an app that helps you plan your expenses realistically.
        By forecasting your bank account balance for a year, you will see if your budget is going to work.
        You'll be avoiding overdraft fees. You'll see where you need to cutback.
        </p>
    </PaddedContent>
)

export default HomePage;