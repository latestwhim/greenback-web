import React from 'react';
import { Segment, Header } from 'semantic-ui-react'

const EmptyLayout = ({ content: Content, route, }) => (
    <React.Fragment>
        <Segment inverted textAlign='center' style={{ padding: '1em 0em' }} vertical>
            <Header as="h1" inverted>Greencast</Header>
        </Segment>
        <Content key={route.match.url} {...route} />
    </React.Fragment>
)

export default EmptyLayout