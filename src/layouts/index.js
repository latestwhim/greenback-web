import React from 'react';
import MainLayout from './MainLayout'
import EmptyLayout from './EmptyLayout'

export const main = component => route =>
    <MainLayout content={component} route={route} />

export const empty = component => route =>
    <EmptyLayout content={component} route={route} />