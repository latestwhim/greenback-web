import React from 'react';
import { NavLink } from 'react-router-dom';
import { Button, Grid, Menu, Segment, Header } from 'semantic-ui-react'

const MainLayout = ({ content: Content, route, }) => (
    <React.Fragment>
        <Segment inverted textAlign='center' vertical>
            <Grid columns={3} verticalAlign='middle'>
                <Grid.Column>
                    <Header as="h1" inverted>Greencast</Header>
                </Grid.Column>
                <Grid.Column>
                    <Menu inverted stackable secondary>
                    <Menu.Item as={NavLink} to='/' exact>Home</Menu.Item>
                    <Menu.Item as={NavLink} to='/about' exact>About</Menu.Item>
                    <Menu.Item as={NavLink} to='/protected' exact>Start</Menu.Item>
                    </Menu>
                </Grid.Column>
                <Grid.Column>
                    <Button size='tiny' inverted as={NavLink} to='/login' exact>
                        Log in
                    </Button>
                    <Button size='tiny' inverted style={{ marginLeft: '0.5em' }}>
                        Sign Up
                    </Button>
                </Grid.Column>
            </Grid>
        </Segment>
        <Content key={route.match.url} {...route} />
    </React.Fragment>
)

export default MainLayout