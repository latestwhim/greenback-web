import React from 'react';
import { withRouter } from 'react-router-dom';

const auth = {
  isAuthenticated: false,
  authenticate(cb) {
    this.isAuthenticated = true
    setTimeout(cb, 100)
  },
  signout(cb) {
    this.isAuthenticated = false
    setTimeout(cb, 100)
  }
}

const AuthButton = withRouter(({ history }) => (
  auth.isAuthenticated ? (
      <p>
      Welcome! <button onClick={() => {
          auth.signout(() => history.push('/'))
      }}>Sign out</button>
      </p>
  ) : (
      <p>You are not logged in.</p>
  )
))


export {
  auth,
  AuthButton
}