import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { main, empty } from './layouts'

import HomePage from './pages/Home';
import Main from './pages/Main';

import PaddedContent from './ui/PaddedContent'

import './App.css';

const App = () => (
  <Switch>
    <Route exact path="/" component={main(HomePage)}/>
    <Route exact path="/about" component={main(About)}/>
    <Route exact path="/login" component={empty(Login)}/>
    <Route exact path='/protected' component={main(Main)}/>
    <Route component={main(NotFound)}/>
  </Switch>
);

const About = () => (
  <PaddedContent>
    <h2>About</h2>
    <p>More information about the app...</p>
  </PaddedContent>
);

const Login = () => (
  <PaddedContent>
    <h2>Login</h2>
    <p>Go ahead...</p>
  </PaddedContent>
);

const NotFound = () => (
  <PaddedContent>
    <h2>404</h2>
    <p>Look inside yourself.</p>
  </PaddedContent>
);

export default App;
