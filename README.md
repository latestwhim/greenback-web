Greenback

The web app using React.

### TODOS
- figure out how to write the data access layer (Redux?)
- figure out how to locally point to data on another port
- build out model (users, portfolios, accounts, expenses, deposits, projections)
- File System / S3 abstraction
- fake auth
- Cognito
- LESS?
- Figure out graphing (D3? SVG?)

#### terms?
- expenses/withdrawls/outflow/egress
- income/deposits/ingress

### development

REACT
https://camjackson.net/post/9-things-every-reactjs-beginner-should-know
https://reactjs.org/docs/composition-vs-inheritance.html

react-router
https://blog.pusher.com/getting-started-with-react-router-v4/
https://tylermcginnis.com/react-router-protected-routes-authentication/
https://simonsmith.io/reusing-layouts-in-react-router-4/

AWS
https://serverless-stack.com/chapters/login-with-aws-cognito.html
https://serverless-stack.com/#deploy-react

LESS
https://www.npmjs.com/package/react-app-rewire-less
https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-a-css-preprocessor-sass-less-etc

DECORATORS
https://medium.com/@gigobyte/enhancing-react-components-with-decorators-441320e8606a

HOOKs
https://github.com/guilleasz/RouteHook/blob/master/src/RouteHook.jsx
npm install react-route-hook --save

UI
https://github.com/wwayne/react-tooltip
npm i -save shortid
let id = shortid.generate()
data-tip data-for={id}

FONTS
https://mediatemple.net/blog/tips/creating-implementing-icon-font-tutorial/
https://fontawesome.com/how-to-use/on-the-web/using-with/react
```
    <span className="fa-layers fa-fw">
        <FontAwesomeIcon icon={faIdBadge} />
        <FontAwesomeIcon icon={faCommentAlt} size='xs' />
</span>
```